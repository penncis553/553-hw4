// Name:
// PennKey:

/* -*- P4_16 -*- */
#include <core.p4>
#include <v1model.p4>

#define CPU_PORT 255


/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/

header ethernet_t {
    bit<48> dstAddr;
    bit<48> srcAddr;
    bit<16> etherType;
}

header ipv4_t {
    // TODO
}

header arp_t {
    // TODO
}

// Each element of the distance vector in `data` is:
// bit<32> prefix
// bit<8>  pfx_length
// bit<8>  cost
// Total: 48 bits / 6 bytes (max 42 entries)
header distance_vec_t {
    bit<32>     src;
    bit<16>     length;
    bit<2016>   data;
}


// Overall structure of the packet's headers
struct headers_t {
    ethernet_t      ethernet;
    ipv4_t          ipv4;
    arp_t           arp;
    distance_vec_t  distance_vec;
}


// Declare local variables here
struct local_variables_t {
    bit<1> forMe;
    // TODO
}


// Digest formats
struct routing_digest_t {
    bit<32>     src;
    bit<16>     length;
    bit<2016>   data;
}

struct arp_digest_t {
    // TODO
}

/*************************************************************************
***********************  P A R S E   P A C K E T *************************
*************************************************************************/

parser cis553Parser(packet_in packet,
                    out headers_t hdr,
                    inout local_variables_t metadata,
                    inout standard_metadata_t standard_metadata) {
    state start {
        transition select(standard_metadata.ingress_port) {
            CPU_PORT: parse_routing;
            default: parse_ethernet;
        }
    }

    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            0x0800: parse_ipv4;
            0x0806: parse_arp;
            0x0553: parse_routing;
            default: accept;
        }
    }

    state parse_arp {
        packet.extract(hdr.arp);
        transition accept;
    }

    state parse_ipv4 {
        packet.extract(hdr.ipv4);
        transition accept;
    }

    state parse_routing {
        packet.extract(hdr.distance_vec);
        transition accept;
    }
}


/*************************************************************************
***********************  I N G R E S S  **********************************
*************************************************************************/

control cis553Ingress(inout headers_t hdr,
                      inout local_variables_t metadata,
                      inout standard_metadata_t standard_metadata) {
    action aiForMe() {
        metadata.forMe = 1;
    }

    action aDrop() {
        mark_to_drop(standard_metadata);
    }

    // Our router is an endpoint on every Ethernet LAN where it is attached.
    // Just like other endpoints, e.g., hosts, it will only listen for L2
    // messages that are addressed to it.
    table tiHandleIncomingEthernet {
        key = {
            hdr.ethernet.dstAddr : exact;
            standard_metadata.ingress_port : exact;
        }
        actions = {
            aiForMe;
            aDrop;
        }
    }

    // Called when we get a routing message from the control plane.  Send it out
    // to all of our neighbors.
    table tiHandleOutgoingRouting {
        // TODO
    }


    // Look up the IP of the next hop on the route using a Longest Prefix Match
    // (lpm) rather than an exact match
    table tiHandleIpv4 {
        // TODO
    }

    // Given an IP, look up the destination MAC address and physical port.
    // This serves the same purpose as the hosts' ARP tables and L2
    // encapsulation.
    table tiHandleOutgoingEthernet {
        // TODO
    }

    // Respond to ARP requests for our IP to tell the requester that packets
    // sent to our IP should be addressed, at Layer-2, to our MAC address.
    table tiHandleIncomingArpReqest {
        // TODO
    }

    // Handle a response to our prior ARP request.  Adding this new entry to the
    // tiHandleOutgoingEthernet lookup table should probably involve the control
    // plane.
    table tiHandleIncomingArpResponse {
        // TODO
    }

    // Called when the packet has both an Ethernet header and is a routing
    // message.  That means it's from another host and we should send it to the
    // control plane.
    table tiHandleIncomingRouting {
        // TODO
    }

    apply {
        // Check the first header
        if (hdr.ethernet.isValid()) {
            tiHandleIncomingEthernet.apply();
        } else {
            // routing packets from the control plane come raw (no L2 header)
            tiHandleOutgoingRouting.apply();
        }

        // Check the second header if the Ethernet dst is us
        if (metadata.forMe == 0) {
            // Don't do anything with it
        } else if (hdr.ipv4.isValid()) {
            tiHandleIpv4.apply();
            tiHandleOutgoingEthernet.apply();
        } else if (hdr.arp.isValid() && hdr.arp.oper == 1) {
            tiHandleIncomingArpReqest.apply();
        } else if (hdr.arp.isValid() && hdr.arp.oper == 2) {
            tiHandleIncomingArpResponse.apply();
        } else if (hdr.distance_vec.isValid()) {
            tiHandleIncomingRouting.apply();
        } else {
            aDrop();
        }
    }
}


/*************************************************************************
***********************  E G R E S S  ************************************
*************************************************************************/

control cis553Egress(inout headers_t hdr,
                     inout local_variables_t metadata,
                     inout standard_metadata_t standard_metadata) {
    apply { }
}


/*************************************************************************
************   C H E C K S U M    V E R I F I C A T I O N   *************
*************************************************************************/

control cis553VerifyChecksum(inout headers_t hdr,
                             inout local_variables_t metadata) {
     apply { }
}


/*************************************************************************
*************   C H E C K S U M    C O M P U T A T I O N   ***************
*************************************************************************/

control cis553ComputeChecksum(inout headers_t hdr,
                              inout local_variables_t metadata) {
    // The switch handles the Ethernet checksum, so we don't need to deal with
    // that, but we do need to deal with the IP checksum!
    apply {
        update_checksum(
            hdr.ipv4.isValid(),
            { hdr.ipv4.version,
              hdr.ipv4.ihl,
              hdr.ipv4.diffserv,
              hdr.ipv4.totalLen,
              hdr.ipv4.identification,
              hdr.ipv4.flags,
              hdr.ipv4.fragOffset,
              hdr.ipv4.ttl,
              hdr.ipv4.protocol,
              hdr.ipv4.srcAddr,
              hdr.ipv4.dstAddr },
            hdr.ipv4.hdrChecksum,
            HashAlgorithm.csum16);
    }
}


/*************************************************************************
****************  E G R E S S   P R O C E S S I N G   ********************
*************************************************************************/

control cis553Deparser(packet_out packet, in headers_t hdr) {
    apply {
        packet.emit(hdr.ethernet);
        packet.emit(hdr.arp);
        packet.emit(hdr.ipv4);
        packet.emit(hdr.distance_vec);
    }
}


/*************************************************************************
***********************  S W I T C H  ************************************
*************************************************************************/

V1Switch(cis553Parser(),
         cis553VerifyChecksum(),
         cis553Ingress(),
         cis553Egress(),
         cis553ComputeChecksum(),
         cis553Deparser()) main;
